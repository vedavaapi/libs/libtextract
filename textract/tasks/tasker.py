import json
import uuid
from timeit import default_timer

import requests
from celery.exceptions import Ignore
from textract import books
from textract.tasks.celery_helper import default_update_state
from vedavaapi.client import VedavaapiSession


def register_task(vs: VedavaapiSession, rtask: dict):
    reg_resp = vs.post('tasker/v1/manager/tasks', data={"tasks": json.dumps([rtask])})
    try:
        reg_resp.raise_for_status()
    except requests.HTTPError as e:
        return {
           "status": "FAILURE",
           "error": {"message": "error in registering task", "reason": e.response.text, "code": 400}}
    reg_status = reg_resp.json()[0]
    return reg_status


def update_task_state(
        vs: VedavaapiSession, task_info: dict, state: str, work_update: dict=None, task_meta=None):
    data = {"state": state, "task_token": task_info['task_token']}
    if work_update: data['work_update'] = json.dumps(work_update)
    if task_meta: data['meta'] = json.dumps(task_meta)
    return vs.put('tasker/v1/manager/tasks', data=data)


def register_task_and_respond(vs: VedavaapiSession, rtask: dict):
    task_id = rtask['status']['id']
    reg_resp = register_task(vs, rtask)
    if reg_resp['status'] != 'SUCCESS':
        eresp = ({"error": reg_resp['error']}, reg_resp['error'].get('code', 400))
        return None, None, None, eresp
    reg_info = reg_resp['info']

    sresp = {
        "status_url": rtask['status']['url'],
        "task_id": task_id, "reg_id": reg_info['task_id']}, 202, {
        'Location': rtask['status']['url'], 'Access-Control-Expose-Headers': 'Location'
    }
    return task_id, reg_resp['info'], sresp, None

def register_page_ocr_task(
        vs: VedavaapiSession, page_id, task_url_fn, name='OCR', service='ocr', work_ext=None):
    task_id = uuid.uuid4().hex
    rtask = {
        "jsonClass": "Task",
        "type": "rdfs:Resource",  # TODO
        "name": name,
        "work": {"work_type": "ocr", "quantum": "page", "quantity": 1},
        "on": {
            "target": {"id": page_id},
            "path": [
                {"target": {"id": page_id}}
            ]
        },
        "status": {
            "id": task_id,
            "state": "PENDING",
            "url": task_url_fn(task_id)
        },
        "actions": ["read", "updateContent", "delete"],
        "service": {
            "name": service
        },
    }

    if work_ext:
        rtask['work']['work_ext'] = work_ext

    return register_task_and_respond(vs, rtask)


def register_bulk_ocr_task(
        vs: VedavaapiSession, book_id, task_url_fn,
        name='OCR on book', service='ocr', work_ext=None, range=None):
    task_id = uuid.uuid4().hex
    rtask = {
        "jsonClass": "Task",
        "type": "rdfs:Resource",  # TODO
        "name": name,
        "work": {"work_type": "ocr", "work_ext": ["google-cv"], "quantum": "page", "quantity": 1},
        "on": {
            "target": {"id": book_id},
            "path": [
                {"target": {"id": book_id}}
            ]
        },
        "status": {
            "id": task_id,
            "url": task_url_fn(task_id),
            "state": "PENDING",
        },
        "actions": ["read", "updateContent", "delete"],
        "service": {
            "name": service
        },
    }
    if work_ext:
        rtask['work']['work_ext'] = work_ext
    if range:
        rtask['on']['range'] = range

    return register_task_and_respond(vs, rtask)


def wrap_task_in_tasker_flow(vs: VedavaapiSession, reg_info: dict, task_fn):
    update_task_state(vs, reg_info, 'PROGRESS')
    try:
        success_meta = task_fn()
        update_task_state(
            vs, reg_info, 'SUCCESS',
            work_update={"succeeded": 1, "failed": 0}, task_meta=success_meta)
        return success_meta
    except Ignore as e:
        update_task_state(
            vs, reg_info, 'FAILURE',
            work_update={"failed": 1, "succeeded": 0}, task_meta={"status": str(e)}
        )
        raise e


def wrap_bulk_task_in_tasker_flow(
        vs: VedavaapiSession, reg_info: dict, bulk_task_fn,
        book_id=None, pages_range=None, page_ids=None, update_state=None):

    update_state = update_state or default_update_state
    update_state(state='PROGRESS', meta={"status": "computing page indices"})

    try:
        resolved_page_ids = books.resolve_page_ids(
            vs, book_id=book_id, page_ids=page_ids, pages_range=pages_range,
            update_state=update_state)
        update_task_state(
            vs, reg_info, 'PROGRESS', work_update={"quantity": len(resolved_page_ids)})
    except Exception as e:
        update_task_state(
            vs, reg_info, 'FAILURE',
            work_update={"failed": 1, "succeeded": 0}, task_meta={"status": str(e)}
        )
        raise e

    start_time = default_timer()

    try:
        succeeded, failed = bulk_task_fn(resolved_page_ids)
    except Exception as e:
        update_task_state(
            vs, reg_info, 'FAILURE',
            work_update={"failed": len(resolved_page_ids), "succeeded": 0},
            task_meta={"status": str(e)}
        )
        raise e

    success_meta = {
        "status": "segmenting complete",
        "current": len(resolved_page_ids), "total": len(resolved_page_ids),
        "succeeded": len(succeeded), "failed": len(failed),
        "failed_ids": failed,
        "time": default_timer() - start_time
    }
    update_task_state(
        vs, reg_info, 'SUCCESS',
        work_update={"succeeded": len(succeeded), "failed": len(failed)},
        task_meta=success_meta
    )
    return success_meta
