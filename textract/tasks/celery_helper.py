import os

from celery.exceptions import Ignore


def default_update_state(state='PROGRESS', meta=None):
    pass


def get_accessible_update_state(container):
    return lambda state='PROGRESS', meta=None: container.update({'state': state, 'meta': meta})


def get_sub_task_update_state(
        update_state, sub_task_context, master_state, master_status, sub_task_key=None, custom_handler=None):
    sub_task_context['progress'] = sub_task_context.get('progress', {})
    progress = sub_task_context['progress']

    def _st_update_state(state=None, meta=None):
        master_meta = {"status": master_status}
        master_meta.update(progress)
        if sub_task_key:
            master_meta[sub_task_key] = {
                "state": state,
                "meta": meta
            }
        if callable(custom_handler):
            custom_handler(master_meta)
        update_state(state=master_state, meta=master_meta)

    return _st_update_state


class AccessableStateUpdater(object):

    def __init__(self, update_state=None):
        self.state = None
        self.meta = None
        self.update_state = update_state

        self._updater = None

    @property
    def updater(self):
        if self._updater:
            return self._updater

        def _updater(state=None, meta=None):
            self.state = state or 'SUCCESS'
            self.meta = meta or {}

            if self.update_state:
                self.update_state(state=state, meta=meta)

        self._updater = _updater
        return self._updater


class VedavaapiRuntimeException(Ignore):

    def __init__(self, update_state=None, exc_message=None, exc_type=None, exception=None):
        super().__init__()
        if update_state:
            update_state(state='FAILURE', meta={
                "exc_type": exc_type,
                "exc_message": exc_message if isinstance(exc_message, list) else [exc_message],
            })
        self.message = exc_message
        self.exception = exception
        self.exc_type = exc_type

    @classmethod
    def from_updater(cls, updater: AccessableStateUpdater):
        return cls(update_state=None, exc_message=updater.meta.get('exc_message'), exc_type=updater.meta.get('exc_type'))


def make_celery(celery_app_path, include_modules):
    from celery import Celery
    broker_url = os.environ.get('CELERY_BROKER_URL', 'redis://')
    backend_url = os.environ.get('CELERY_RESULT_BACKEND', 'redis://')

    celery_app = Celery(
        celery_app_path,
        broker=broker_url,
        backend=backend_url,
        include=include_modules[:]
    )
    celery_app.conf.update(
        result_expires=86400,
        accept_content=['pickle'],
        result_serializer='pickle',
        task_serializer='pickle'
    )

    return celery_app


def get_task_status(celery_app, task_id):
    result = celery_app.AsyncResult(task_id)
    if not result:
        return None

    #  print({"task_info": result.info})
    if result.state == 'PENDING':
        # job did not start yet
        response = {
            'state': result.state,
            'status': 'Pending...',
        }

    elif result.state != 'FAILURE':
        response = {
            'state': result.state
        }
    else:
        print(vars(result))
        # something went wrong in the background job
        response = {
            'state': result.state,
            'status': str(result.info),  # this is the exception raised
        }

    if isinstance(result.info, dict):
        response.update(result.info)

    return response


def terminate_task(celery_app, task_id):
    return celery_app.control.revoke(task_id, terminate=True, signal='SIGUSR1')


def get_task_url_fn(api, status_endpoint):
    return lambda task_id: api.url_for(status_endpoint, task_id=task_id, _external=True)
