import json
import re
from typing import Union, Dict, List

import requests
from requests import HTTPError

from vedavaapi.client import VedavaapiSession

from .. import books
from ..books.segmenters.hocr_models import BoundingBox
from ..tasks.celery_helper import VedavaapiRuntimeException

'''
Following are utilitarian models for few vv objects related to textract
'''


class JsonObject(object):

    def __init__(self, object_json):
        self.json = object_json


class Resource(JsonObject):

    def __init__(self, resource_json, vc=None, update_state=None):
        super().__init__(resource_json)
        self.vc = vc  # type: VedavaapiSession
        self._id = self.json.get('_id')
        self._update_state = update_state

    @classmethod
    def resource_url(cls, resource_id):
        return 'objstore/v1/resources/{}'.format(resource_id) if resource_id else None

    @classmethod
    def file_url(cls, oold_id):
        return 'objstore/v1/files/{}'.format(oold_id) if oold_id else None

    # noinspection PyUnusedLocal
    @classmethod
    def get_file_content(cls, vc, oold_id, update_state=None):
        return books.get_oold(vc, oold_id)

    @property
    def url(self):
        if not self.vc:
            return None
        return self.vc.abs_url(self.resource_url(self._id))

    @classmethod
    def load(cls, vc, _id, projection=None, update_state=None):
        if not vc or not _id:
            return None

        params = None
        if projection:
            params = {'projection': json.dumps(projection)}

        try:
            response = vc.get(cls.resource_url(_id), parms=params)  # type: requests.Response
            response.raise_for_status()
        except HTTPError as e:
            raise VedavaapiRuntimeException(
                update_state=update_state,
                exc_type='HTTPError',
                exc_message=["error in getting page info; code: {}; error: {}".format(
                    e.response.status_code, e.response.json())
                ],
                exception=e
            )

        resource_json = response.json()
        return cls(resource_json, vc=vc, update_state=update_state)

    @classmethod
    def get_repr(cls, resource_json, repr_type):
        if not json:
            return None
        reprs = resource_json.get('representations', {}).get(repr_type, [])
        if not len(reprs):
            return None
        reprn = reprs[0]
        return reprn

    @classmethod
    def get_repr_oold_id(cls, resource_json, repr_type):
        reprn = cls.get_repr(resource_json, repr_type)
        if not reprn:
            return None
        oold_id = reprn.get('data')  # type: str
        if not oold_id:
            return None
        if not oold_id.startswith('_OOLD:'):
            return None
        return oold_id[6:]


class ScannedPage(Resource):

    def __init__(self, resource_json, vc=None, update_state=None):
        super().__init__(resource_json, vc=vc, update_state=update_state)
        self.regions_graph: Union[Dict[str, Dict], None] = None
        self.layout_graph: Union[Dict[str, Dict], None] = None
        self.ordered_regions: Union[List[Dict], None] = None

        self._image_id: Union[str, None] = None
        self._image_content: Union[bytes, None] = None
        self._image_oold: Union[Resource, None] = None

    @property
    def image_id(self):
        if self._image_id:
            return self._image_id
        if not self.json:
            return None
        self._image_id = self.get_repr_oold_id(self.json, 'stillImage')
        return self._image_id

    @property
    def image_url(self):
        if not self.image_id or not self.vc:
            return None
        return self.vc.abs_url(self.file_url(self.image_id))

    @property
    def image_oold(self):
        if not self._image_oold:
            self._image_oold = Resource.load(self.vc, self.image_id, update_state=self._update_state)
        return self._image_oold

    @property
    def image_content(self):
        if self._image_content:
            return self._image_content
        if not self.image_id:
            return None
        self._image_content = self.get_file_content(self.vc, self.image_id, update_state=self._update_state)
        return self._image_content

    # to get image definition in single call
    @classmethod
    def load_with_oold_def(cls, vc, _id, projection=None, update_state=None):
        if not vc or not _id:
            return None

        try:
            response = vc.put('objstore/v1/graph', data={
                "start_nodes_selector": json.dumps({"_id": _id}),
                "include_ool_data_graph": 'true',
                "json_class_projection_map": json.dumps(
                    {"ScannedPage": projection, "*": {"resolvedPermissions": 0}}),
                "direction": "referrer",
            })  # type: requests.Response
            response.raise_for_status()
        except HTTPError as e:
            raise VedavaapiRuntimeException(
                update_state=update_state,
                exc_type='HTTPError',
                exc_message=["error in getting page info; code: {}; error: {}".format(
                    e.response.status_code, e.response.json())
                ],
                exception=e
            )

        response_json = response.json()
        if _id not in response_json['start_nodes_ids']:
            raise VedavaapiRuntimeException(
                update_state=update_state,
                exc_type='ObjModelException',
                exc_message=["cannot access resource with given _id"],
            )

        res = cls(response_json['graph'][_id], vc=vc, update_state=update_state)
        res._image_oold = response_json['graph'].get(res.image_id, None)
        return res

    def load_regions(self, include_annos=False):
        if not self.vc or not self._id:
            return None
        self.regions_graph, self.layout_graph = books.get_regions_layout_graphs(
            self.vc, [self._id], include_annos=include_annos, update_state=self._update_state)
        self.ordered_regions = [
            ImageRegion(reg_json, vc=self.vc, update_state=self._update_state)
            for reg_json in books.get_ordered_regions(self._id, self.layout_graph, self.regions_graph) or []
        ]
        return self.ordered_regions

    def release(self):
        self._image_content = None
        self.layout_graph = None
        self.regions_graph = None
        self.ordered_regions = None


class ImageRegion(Resource):

    # noinspection RegExpRedundantEscape
    FRAGMENT_RE = re.compile(r'xywh=(?P<x>[0-9\.]+),(?P<y>[0-9\.]+),(?P<w>[0-9\.]+),(?P<h>[0-9\.]+)')

    def __init__(self, resource_json, vc=None, update_state=None):
        super().__init__(resource_json, vc=vc, update_state=update_state)

        self._fragment_selector = None
        self._svg_selector = None

        self._bbox = None
        self._svg = None

    def _get_selectors(self):
        if not self.json:
            return None, None
        selector = self.json.get('selector')
        if not selector or selector.get('jsonClass') not in (
                'FragmentSelector', 'SvgSelector', 'SvgFragmentSelectorChoice'):
            return None, None
        selector_jc = selector['jsonClass']
        if selector_jc == 'FragmentSelector':
            self._fragment_selector = selector
        elif selector_jc == 'SvgSelector':
            self._svg_selector = selector
        else:
            self._fragment_selector = selector['default']
            self._svg_selector = selector['item']
        return self._fragment_selector, self._svg_selector

    @property
    def fragment_selector(self):
        self._get_selectors()
        return self._fragment_selector

    @property
    def svg_selector(self):
        self._get_selectors()
        return self._svg_selector

    @property
    def bbox(self):
        if self._bbox:
            return self._bbox

        if not self.fragment_selector:
            return None

        fragment = self.fragment_selector.get('value')
        if not fragment:
            return None
        match = self.FRAGMENT_RE.fullmatch(fragment)
        if not match:
            return None
        self._bbox = BoundingBox(
            float(match.group('x')),
            float(match.group('y')),
            float(match.group('w')),
            float(match.group('h')),
        )
        return self._bbox

    @property
    def svg(self):
        if self._svg:
            return self._svg

        if not self.svg_selector:
            return None

        self._svg = self.svg_selector.get('value')
        return self._svg
