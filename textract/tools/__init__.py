import functools
from ..tasks.celery_helper import get_sub_task_update_state

def batch_it(
        batch_size, result_reducer=None, batch_context=None,
        update_state=None, master_status=None, master_state='PROGRESS',
        branch_state_key=None, branch_state_handler=None):
    batch_context = batch_context or {}

    def wrapper(func):
        @functools.wraps(func)
        def decorated(all_items, *args, **kwargs):
            if not isinstance(all_items, list):
                return func(all_items, *args, **kwargs)
            reduced_result = None
            no_batches = len(all_items) // batch_size + 1

            batch_context['progress'] = batch_context.get('progress', {"current": 0, "total": all_items})
            progress = batch_context['progress']
            batch_update_state = get_sub_task_update_state(
                update_state, batch_context, master_state, master_status,
                sub_task_key=branch_state_key, custom_handler=branch_state_handler
            ) if update_state else None

            for batch_count in range(no_batches):
                batch_context.update({
                    "current_batch": batch_count + 1, "total_batches": no_batches,
                    "batch_size": batch_size, "total_items": len(all_items), "break": False
                })
                start = batch_count * batch_size
                batch = all_items[start: start + batch_size]
                if not len(batch):
                    break

                result = func(batch, *args, **kwargs, batch_context=batch_context, update_state=batch_update_state)
                if callable(result_reducer):
                    reduced_result = result_reducer(reduced_result, result) if batch_count > 0 else result

                progress.update({
                    "current": len(all_items) if batch_count == no_batches - 1 else ((batch_count + 1) * batch_size),
                    "total": len(all_items)
                })
                if update_state:
                    update_state(
                        state=master_state,
                        meta={"status": master_status, "current": progress['current'], "total": progress['total']}
                    )

                if batch_context.get('break') is True:
                    break
            return reduced_result
        return decorated
    return wrapper
