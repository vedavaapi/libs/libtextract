import json
import re

import requests
from requests import HTTPError
from vedavaapi.client import VedavaapiSession, objstore

from ..tasks.celery_helper import default_update_state, VedavaapiRuntimeException


def get_page_ids(vc: VedavaapiSession, book_id, pages_range=None, update_state=None):
    if book_id is None:
        return None

    update_state = update_state or default_update_state

    #  print('getting book graph')
    start_nodes_selector = {"_id": book_id}
    traverse_key_filter_maps_list = [
        {
            "target": {"jsonClass": "SequenceAnnotation", "canonical": "default_canvas_sequence"}
        }
    ]
    try:
        response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction='referrer', max_hops=1,
            json_class_projection_map={
                "*": {"_id": 1, "jsonClass": 1, "selector": 1, "index": 1, "_reached_ids": 1, "body": 1}
            }
        )
        #  print("got book graph")
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['error in retrieving book graph {}'.format(str(e))],
            exc_type='HTTPError',
            exception=e,
        )

    response_graph = response['graph']
    book = response_graph.get(book_id)
    if not book:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['book with _id {} is not accessible'.format(book_id)],
            exc_type='ObjModelException',
        )

    seq_anno_ids = book.get('_reached_ids', {}).get('target', None)
    if isinstance(seq_anno_ids, list) and len(seq_anno_ids):
        seq_anno_id = seq_anno_ids[0]
        seq_anno = response_graph[seq_anno_id]
        seq_anno_members = seq_anno.get('body', {}).get('members', [])
        sequenced_ids = [m['resource'] for m in seq_anno_members]
    else:
        selector_doc = {"jsonClass": "ScannedPage", "source": book_id}
        projection = {"selector": 1, "index": 1, "_id": 1}
        pages_resp = vc.get(
            'objstore/v1/resources',
            parms={"selector_doc": json.dumps(selector_doc), "projection": json.dumps(projection)})
        try:
            pages_resp.raise_for_status()
        except HTTPError as e:
            raise VedavaapiRuntimeException(
                update_state=update_state,
                exc_message=['error in retrieving pages of book {}: {}'.format(book_id, str(e))],
                exc_type='HTTPError',
            )
        pages = pages_resp.json()['items']
        sequenced_ids = [p['_id'] for p in pages]

    if (not isinstance(pages_range, list)
            or not len(pages_range) == 2
            or False in [isinstance(b, int) for b in pages_range]):
        return sequenced_ids

    return sequenced_ids[pages_range[0]: pages_range[1]]


def resolve_page_ids(vc, book_id=None, page_ids=None, pages_range=None, update_state=None):
    update_state = update_state or default_update_state

    if page_ids is not None:
        resolved_page_ids = page_ids
    else:
        update_state(state='PROGRESS', meta={
            "status": "getting page ids"
        })
        resolved_page_ids = get_page_ids(
            vc, book_id, pages_range=pages_range, update_state=update_state
        )

    if resolved_page_ids is None:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['invalid page ids'],
            exc_type='PolicyError',
        )
    return resolved_page_ids


def get_regions_graph(
        vc: VedavaapiSession, page_ids, include_annos=True, include_annos_history=False,
        json_class_projection_map=None, update_state=None):

    update_state = update_state or default_update_state
    start_nodes_selector = {"jsonClass": "ScannedPage", "_id": {"$in": page_ids}}

    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "ImageRegion"}},
    ]
    max_hops = 1
    if include_annos:
        max_hops += 1
        traverse_key_filter_maps_list.append({"target": {"jsonClass": "TextAnnotation"}})
        if include_annos_history:
            max_hops += 1
            traverse_key_filter_maps_list.append({"target": {"jsonClass": "SnapshotAnnotation"}})

    direction = 'referrer'
    json_class_projection_map = json_class_projection_map or {"*": {"resolvedPermissions": 0}}

    try:
        #  print('getting regions graph')
        regions_graph_response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction=direction, max_hops=max_hops,
            json_class_projection_map=json_class_projection_map
        )
        regions_graph = regions_graph_response['graph']
        #  print('regions graph retrieved')
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['error in retrieving regions graph: {}'.format(str(e))],
            exc_type='HTTPError',
            exception=e,
        )

    return regions_graph


def get_layout_graph(vc: VedavaapiSession, page_ids, update_state=None):
    update_state = update_state or default_update_state
    start_nodes_selector = {"_id": {"$in": page_ids}, "jsonClass": "ScannedPage"}
    '''
    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRPage"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRCArea"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRParagraph"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRLine"}}
    ]
    '''
    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "Resource", "jsonClassLabel": {"$regex": "^HOCR"}}}
    ]

    direction = 'referrer'
    max_hops = 4
    json_class_projection_map = {
        "*": {"_id": 1, "jsonClass": 1, "index": 1, "members": 1, "source": 1, "_reached_ids": 1}
    }

    try:
        #  print('getting layout graph')
        layout_graph_response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction=direction, max_hops=max_hops,
            json_class_projection_map=json_class_projection_map
        )
        layout_graph = layout_graph_response['graph']
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['error in retrieving layout graph: {}'.format(str(e))],
            exc_type='HTTPError',
            exception=e,
        )

    return layout_graph


def get_regions_layout_graphs(
        vc: VedavaapiSession, page_ids, include_annos=True,
        include_annos_history=False, json_class_projection_map=None, update_state=None):

    update_state = update_state or default_update_state
    regions_graph = get_regions_graph(
        vc, page_ids, include_annos=include_annos, include_annos_history=include_annos_history,
        json_class_projection_map=json_class_projection_map, update_state=update_state)
    try:
        layout_graph = get_layout_graph(vc, page_ids, update_state=None)
    except VedavaapiRuntimeException:
        # print('no layout exists')
        layout_graph = None
    return regions_graph, layout_graph


def region_sort_key_fn(region):
    fragment_selector = region['selector']['default']
    fragment_val = fragment_selector['value']
    fragment_val_regex = r'xywh=(?P<x>[0-9]+),(?P<y>[0-9]+),(?P<w>[0-9]+),(?P<h>[0-9]+)'
    x, y, w, h = re.match(fragment_val_regex, fragment_val).groups()
    return int(y), int(x)


def ordered_regions_by_co_ords(regions):
    sorted_regions = sorted(regions, key=region_sort_key_fn)
    return sorted_regions


def get_ordered_regions(page_id, layout_graph, regions_graph):

    page_v1 = regions_graph.get(page_id)
    if not page_v1:
        return None

    region_ids = page_v1.get('_reached_ids', {}).get('source', None)

    if region_ids is None:
        return None

    def _default_ordered_regions():
        _regions = [regions_graph[rid] for rid in region_ids if rid in regions_graph]
        return ordered_regions_by_co_ords(_regions)

    if not layout_graph:
        return _default_ordered_regions()

    page_v2 = layout_graph.get(page_id)
    if not page_v2 or not page_v2.get('_reached_ids', {}).get('source', None):
        return _default_ordered_regions()

    regions = []

    def _extend_regions(layout_elem):
        if not layout_elem:
            return
        member_resource_ids = layout_elem.get('members', {}).get('resource_ids', None)
        if member_resource_ids:
            regions.extend(
                [regions_graph[rid] for rid in member_resource_ids
                 if rid in regions_graph]
            )
            return
        reached_layout_elem_ids = layout_elem.get('_reached_ids', {}).get('source', None)
        if not reached_layout_elem_ids:
            return
        for reached_elem_id in reached_layout_elem_ids:
            _extend_regions(layout_graph.get(reached_elem_id))

    _extend_regions(page_v2)  # NOTE : page passed
    return regions


def delete_existing_regions(vc, page_id, update_state=None, resources_state=None):
    update_state = update_state or default_update_state
    filter_doc = {"jsonClass": {"$in": ["Resource", "ImageRegion"]}}
    if resources_state:
        filter_doc['state'] = resources_state
    delete_request_data = {"filter_doc": json.dumps(filter_doc)}

    try:
        children_delete_response = vc.delete(
            'objstore/v1/resources/{}/specific_resources'.format(page_id), data=delete_request_data)
        children_delete_response.raise_for_status()
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=["error in deleting existing image regions; code: {}; error: {}".format(
                e.response.status_code, e.response.json())
            ],
            exc_type='HTTPError',
            exception=e,
        )

    response_json = children_delete_response.json()
    if response_json.get('delete_status') and False in response_json.get('delete_status').values():
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=['cannot delete all existing regions'],
            exc_type='PolicyError',
        )


def get_page_image_id(vc, page_id, update_state=None):
    update_state = update_state or default_update_state
    page_url = 'objstore/v1/resources/{}'.format(page_id)
    parms = {"projection": json.dumps({"resolvedPermissions": 0})}

    try:
        response = vc.get(page_url, parms=parms)  # type: requests.Response
        response.raise_for_status()
    except requests.HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=["error in getting page info; code: {}; error: {}".format(
                e.response.status_code, e.response.json())
            ],
            exc_type='HTTPError',
            exception=e,
        )

    resource_json = response.json()
    if 'representations' not in resource_json or 'stillImage' not in resource_json['representations']:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=["no image exists for page"],
            exc_type='PolicyError',
        )

    resource_image_oold_id = resource_json['representations']['stillImage'][0]['data']
    if not resource_image_oold_id.startswith('_OOLD:'):
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message=["page doesn\'t have image representation"],
            exc_type='PolicyError',
        )

    resource_image_oold_id = resource_image_oold_id[6:]
    return resource_image_oold_id


def get_oold_url(vc, oold_id):
    file_url = 'objstore/v1/files/{}'.format(oold_id)
    abs_file_url = vc.abs_url(file_url)
    return abs_file_url


def get_oold(vc, oold_id):
    file_url = 'objstore/v1/files/{}'.format(oold_id)
    file_response = vc.get(file_url)
    file_content = file_response.content
    return file_content
