from typing import List

import json
from collections import namedtuple

from requests import HTTPError
from textract.books.segmenters import svg_helper
from textract.tasks.celery_helper import VedavaapiRuntimeException

Vertex = namedtuple('Vertex', ['x', 'y'])

'''
Following are constructive models for hocr concepts

'''


class BoundingBox(object):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def json(self):
        return {"x": self.x, "y": self.y, "w": self.w, "h": self.h}

    def fragment_value(self):
        return "xywh={}".format(','.join([str(i) for i in [self.x, self.y, self.w, self.h]]))


class Polygon(object):

    def __init__(self, vertices: list):
        if False not in [isinstance(v, Vertex) for v in vertices]:
            self.vertices = vertices.copy()
        elif False not in [hasattr(v, 'x') and hasattr(v, 'y') for v in vertices]:
            self.vertices = [Vertex(v.x, v.y) for v in vertices]
        elif False not in [isinstance(v, dict) for v in vertices]:
            self.vertices = [Vertex(v['x'], v['y']) for v in vertices]
        else:
            raise ValueError('invalid argument')

    @classmethod
    def from_rectangle(cls, x, y, w, h):
        return cls([
            Vertex(x, y),
            Vertex(x + w, y),
            Vertex(x + w, y + h),
            Vertex(x, y + h)
        ])

    @classmethod
    def from_bbox(cls, bbox: BoundingBox):
        return cls.from_rectangle(bbox.x, bbox.y, bbox.w, bbox.h)

    def bbox(self):
        min_x = self.vertices[0].x
        min_y = self.vertices[0].y
        max_x = self.vertices[0].x
        max_y = self.vertices[0].y
        for v in self.vertices[1:]:
            if v.x < min_x:
                min_x = v.x
            if v.y < min_y:
                min_y = v.y
            if v.x > max_x:
                max_x = v.x
            if v.y > max_y:
                max_y = v.y

        return BoundingBox(min_x, min_y, max_x - min_x, max_y - min_y)

    def fragment_selector(self):
        bbox = self.bbox()
        return {
            "jsonClass": "FragmentSelector",
            "value": bbox.fragment_value(),
            "rectangle": bbox.json()
        }

    def svg(self):
        return svg_helper.svg_for_polygon(self.vertices)

    def svg_selector(self):
        return {
            "jsonClass": "SvgSelector",
            "value": self.svg()
        }

    def selector(self):
        return {
            "jsonClass": "SvgFragmentSelectorChoice",
            "default": self.fragment_selector(),
            "item": self.svg_selector()
        }


class HOCRElement(object):

    CLS = 'element'

    def __init__(
            self, _id: str, region: Polygon, parent=None,
            children=None, annos=None, members=None,
            state='system_inferred', index=None):
        self._id = _id
        self.region = region
        self.parent = parent
        self.children = children or []
        self.annos = annos or []
        self.members = members or []
        self.state = state
        self.index = index

    def json(self, members_gid_uid_map=None):
        section = {
            "_id": self._id,
            "jsonClass": "Resource",
            "source": self.parent,
            "selector": self.region.selector(),
            "state": self.state,
            "jsonClassLabel": self.CLS,
            "index": self.index
        }
        if len(self.members):
            members_gid_uid_map = members_gid_uid_map or {}
            # noinspection PyProtectedMember
            section['members'] = dict(resource_ids=[members_gid_uid_map.get(m._id, m._id) for m in self.members])
        remove_nones(section)
        return section

    # noinspection PyProtectedMember
    def get_regions_graph(self, pre_graph=None):
        graph = pre_graph if pre_graph is not None else {}
        for child in self.children:
            child.get_regions_graph(pre_graph=graph)
        for member in self.members:
            graph[member._id] = member.json()
            graph.update(dict((a._id, a.json()) for a in member.annos))
            #  graph.update(member.get_regions_graph())
        return graph

    def get_layout_graph(self, members_graph_uid_map=None, pre_graph=None):
        graph = pre_graph if pre_graph is not None else {}
        self_json = self.json()
        if 'members' in self_json and members_graph_uid_map is not None:
            self_json['members']['resource_ids'] = [
                members_graph_uid_map.get(mid, mid)
                for mid in self_json['members']['resource_ids']
            ]
        graph[self._id] = self_json

        for child in self.children:
            child.get_layout_graph(members_graph_uid_map=members_graph_uid_map, pre_graph=graph)
        return graph

    def get_hierarchy_graph(self, *args, **kwargs):
        return self.get_layout_graph(*args, **kwargs)

    def hierarchy_json(self):
        json_repr = self.json()
        json_repr['members'] = [m.hierarchy_json() for m in self.members]
        json_repr['children'] = [c.hierarchy_json() for c in self.children]
        json_repr['annos'] = [a.json() for a in self.annos]
        return json_repr

    def __repr__(self):
        return json.dumps(self.hierarchy_json(), indent=2)


class HOCRPage(HOCRElement):

    CLS = 'HOCRPage'


class HOCRCArea(HOCRElement):

    CLS = 'HOCRCArea'


class HOCRParagraph(HOCRElement):

    CLS = 'HOCRParagraph'


class HOCRLine(HOCRElement):

    CLS = 'HOCRLine'


class HOCRWord(HOCRElement):

    CLS = 'HOCRWord'

    def json(self, members_gid_uid_map=None):
        section = super(HOCRWord, self).json(members_gid_uid_map=members_gid_uid_map)
        section['jsonClass'] = 'ImageRegion'
        return section


def remove_nones(doc: dict):
    for k in list(doc.keys()):
        if doc[k] is None:
            doc.pop(k)


class TextAnnotation(object):

    def __init__(
            self, _id, text, parent=None, confidence=None, member_confidence=None,
            language=None, script=None, generator=None):
        self._id = _id
        self.text = text
        self.parent = parent
        self.confidence = confidence
        self.member_confidence = member_confidence
        self.language = language
        self.script = script
        self.generator = generator

    def json(self):
        text = {
            "jsonClass": "Text",
            "chars": self.text,
            "language": self.language,
            "script": self.script
        }
        remove_nones(text)
        anno_json = {
            "jsonClass": "TextAnnotation",
            "target": self.parent,
            "_id": self._id,
            "body": [{
                "jsonClass": "Text",
                "chars": self.text
            }],
            "generator": self.generator,
            "confidence": self.confidence,
            "member_confidence": self.member_confidence
        }
        remove_nones(anno_json)
        return anno_json


def persist_hocr_pages(vc, hocr_pages: List[HOCRPage], update_state=None):
    regions_graph = {}
    for hocr_page in hocr_pages:
        hocr_page.get_regions_graph(pre_graph=regions_graph)

    try:
        regions_post_response = vc.post('objstore/v1/graph', data={
            "graph": json.dumps(regions_graph),
            "should_return_resources": "false",
        })
        regions_post_response.raise_for_status()
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message='error in posting regions: {}'.format(str(e)),
            exc_type='HTTPError',
            exception=e,
        )

    regions_gid_uid_map = regions_post_response.json()['graph']

    layout_graph = {}
    for hocr_page in hocr_pages:
        hocr_page.get_layout_graph(
            members_graph_uid_map=regions_gid_uid_map, pre_graph=layout_graph)

    try:
        layout_post_response = vc.post('objstore/v1/graph', data={
            "graph": json.dumps(layout_graph),
            "should_return_resources": "false",
        })
        layout_post_response.raise_for_status()
    except HTTPError as e:
        raise VedavaapiRuntimeException(
            update_state=update_state,
            exc_message='error in posting layout',
            exc_type='HTTPError',
            exception=e,
        )

    return regions_post_response.json(), layout_post_response.json()
