import re
from requests import HTTPError

from vedavaapi.client import VedavaapiSession, objstore

from ... import books
from ...tools import batch_it
from ...tasks.celery_helper import default_update_state


def compute_associations(page_id, graph: dict, page_lines: list):
    #  print(page_id, page_lines)
    page = graph.get(page_id)
    if page is None:
        return None

    associations = []
    reached_ids = page.get('_reached_ids')
    if not reached_ids or not reached_ids.get('source'):
        return associations

    regions = []
    for rid in reached_ids['source']:
        region = graph.get(rid)
        if not region:
            continue
        if not region.get('selector', {}).get('default', {}).get('value'):
            continue
        regions.append(region)

    sorted_regions = sorted(regions, key=books.region_sort_key_fn)

    associations_count = min(len(regions), len(page_lines))

    for i, region in enumerate(sorted_regions[:associations_count]):
        anno_ids = region.get('_reached_ids', {}).get('target', [])
        associated_anno_id = anno_ids[0] if len(anno_ids) else '_:{}-anno'.format(region['_id'])
        anno = {
            "jsonClass": "TextAnnotation",
            "_id": associated_anno_id,
            "body": [{
                "jsonClass": "Text",
                "chars": page_lines[i]
            }],
            "target": region['_id'],
            "generator": "Associater"
        }
        associations.append((region, anno))

    return associations


def compute_associations_graph(vc: VedavaapiSession, page_ids, page_line_collns, update_state=None):
    regions_graph = books.get_regions_graph(vc, page_ids, update_state=update_state)
    associations_graph = {}
    pages_count = min(len(page_ids), len(page_line_collns))

    for index, page_id in enumerate(page_ids[:pages_count]):
        associations = compute_associations(page_id, regions_graph, page_line_collns[index])
        if associations is None:
            continue
        associations_graph.update(dict((anno['_id'], anno) for (region, anno) in associations))

    return associations_graph


def associate(
        vc: VedavaapiSession, lines_stream,
        book_id=None, pages_range=None, page_ids=None, batch_size=20, update_state=None):
    update_state = update_state or default_update_state

    update_state(state='PROGRESS', meta={"status": 'computing page ids'})
    resolved_page_ids = books.resolve_page_ids(vc, book_id=book_id, page_ids=page_ids, pages_range=pages_range)

    status_dict = {
        "succeeded": [],
        "failed": [],
        "message": None,
        #  "errors": {}
    }

    if not resolved_page_ids:
        status_dict['message'] = 'no pages exists'
        return status_dict

    def read_page():
        delimiter_regex = r'^===PAGE[^\n=]*===\n$'
        lines = []
        while True:
            line = lines_stream.readline()
            if not line:
                return lines, True
            if re.match(delimiter_regex, line, re.IGNORECASE):
                return lines, False
            if str(line).startswith('#'):
                continue
            lines.append(line)

    # noinspection PyUnusedLocal
    def associate_pages(_page_ids, batch_context=None, **kwargs):
        #  batch_update_state = kwargs.get('update_state', None)
        pages_lines = []
        exhausted = False
        for i in range(len(_page_ids)):
            page_lines, exhausted = read_page()
            pages_lines.append(page_lines)
            if exhausted:
                break
        associations_graph = compute_associations_graph(vc, _page_ids, pages_lines)
        try:
            objstore.post_graph(vc, associations_graph, should_return_resources=False, upsert=True)
            status_dict['succeeded'].extend(_page_ids)
        except HTTPError as e:
            status_dict['failed'].extend(_page_ids)

        if exhausted and batch_context:
            batch_context['break'] = True

    associate_pages_batch_wise = batch_it(
        batch_size, update_state=update_state,
        master_status='retrieving pages and their annotations', master_state='PROGRESS'
    )(associate_pages)
    associate_pages_batch_wise(resolved_page_ids)
    status_dict['message'] = 'association task completed'
    return status_dict
