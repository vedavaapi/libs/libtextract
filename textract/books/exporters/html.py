from vedavaapi.client import VedavaapiSession

from ... import books
from ...tools import batch_it
from ...tasks.celery_helper import default_update_state


def content_from_regions(
        region_anno_graph, sorted_regions,
        html=True, add_class=True, newline2br=True, add_id=True, add_quotes=True):
    content = ''
    for region in sorted_regions:
        anno_ids = region.get('_reached_ids', {}).get('target', None)
        if not anno_ids:
            continue
        anno_id = anno_ids[0]
        anno = region_anno_graph[anno_id]
        txt = anno.get('body', [{}])[0].get('chars', '')
        if newline2br:
            txt = txt.replace('\n', '<br>')
        q = '"' if add_quotes else ''
        content += '<span{cls}{id}>{txt}</span>'.format(
            cls=' class={q}vv-text-anno{q}'.format(q=q) if add_class else '',
            id=' id={q}{id}{q}'.format(q=q, id=anno_id) if add_id else '',
            txt=txt
        ) if html else anno.get('body', [{}])[0].get('chars', '')
    return content


# noinspection PyUnusedLocal
def export_pages(
        page_ids, vc: VedavaapiSession, update_state=None, **kwargs):

    if not isinstance(page_ids, list):
        return None
    update_state = update_state or default_update_state

    content = ''
    regions_graph, layout_graph = books.get_regions_layout_graphs(
        vc, page_ids, update_state=update_state
    )
    for page_id in page_ids:
        ordered_regions = books.get_ordered_regions(page_id, layout_graph, regions_graph)
        if ordered_regions is None:
            continue
        page_content = content_from_regions(regions_graph, ordered_regions)
        content += page_content

    return content


def export(vc: VedavaapiSession, book_id=None, pages_range=None, page_ids=None, update_state=None, batch_size=15):
    update_state = update_state or default_update_state
    resolved_page_ids = books.resolve_page_ids(
        vc, book_id=book_id, page_ids=page_ids, pages_range=pages_range, update_state=update_state)

    update_state(
        state='PROGRESS', meta={"status": "initializing getting pages content"}
    )

    export_pages_batch_wise = batch_it(
        batch_size, result_reducer=lambda r, n: r + n, update_state=update_state,
        master_state='PROGRESS', master_status='retrieving pages and their annotations')(export_pages)
    content = export_pages_batch_wise(resolved_page_ids, vc, update_state=update_state)
    return content
