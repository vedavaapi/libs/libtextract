from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    long_description = ''

setup(
    name='textract',
    version='1.22',
    packages=['textract', 'textract.books', 'textract.tools', 'textract.tasks'],
    url='https://github.com/vedavaapi/libtextract',
    author='vedavaapi',
    description='textract lib',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['requests', 'celery[redis]', 'tqdm', 'vedavaapi-client'],
    classifiers=(
            "Programming Language :: Python :: 3",
            "Operating System :: OS Independent",
    )
)
