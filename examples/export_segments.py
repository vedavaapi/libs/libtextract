"""

This example takes all page graphs, and convert it to convenient structure like follows.
This structure is ad-hoc and example specific, and can be of any way we want.

{
    api_base: base url of api,
    book_id: book_id,
    pages: [ // an ordered list of structures in following schema
        {
            page: PAGE_JSON,
            segments: [ // an ordered list of structures with following schema
                {
                    region_id: region's _id,
                    region_bbox: ImageRegion's bounding box,
                    region_svg_path_def: ImageRegion svg path definition,
                    segment_url: iiif url of image segment. we can derive it from api base_url, and region though
                    annotation_id: anno's _id,
                    annotation_text: text of TextAnnotation over above region,
                }, ...
            ]
        }, ...
    ]
}


example usage: python3 export_segments.py -a https://services.vedavaapi.org:8443/api -e info@vedavaapi.org -p 1234 -b 5de8e8283516bd00091680ff -o test.json

"""

import argparse
import json
import re
import sys
from tqdm import tqdm  # for progress bar

from textract import books
from textract.tasks.celery_helper import default_update_state
from textract.tools import batch_it
from vedavaapi.client import VedavaapiSession

BBOX_RE = re.compile(r'(?P<x>[0-9\.-]+),(?P<y>[0-9\.-]+),(?P<w>[0-9\.-]+),(?P<h>[0-9\.-]+)$')  # x,y,w,h
SVG_PATH_DEF_RE = re.compile(r' +d *="(?P<d>[^"]+)"')

# noinspection PyUnusedLocal
def export_pages(
        page_ids, vc: VedavaapiSession, **kwargs):
    """
    This fn returns array of page structures as defined in module docstring

    We normally pass like 15 pages in each iteration. This function will be called many times
    with a batch of page_ids in each iteration, until all pages collected.

    As this will interface with api, distributing pages in multiple batch calls is required,
    so that request will not timeout


    :param page_ids: _ids of pages
    :param vc: VedavaapiSession instance
    :param kwargs: other kwargs, will be passed by batching annotator
    :return:
    """
    if not isinstance(page_ids, list):
        return None

    # graphs from api
    segments_graph, layout_graph = books.get_regions_layout_graphs(vc, page_ids)

    # array of page structures as defined in module docstring
    page_export_structures = []
    for page_id in page_ids:
        page = segments_graph[page_id]
        page_image_id = re.match(r'_OOLD:(.*)$', page['representations']['stillImage'][0]['data']).group(1)
        if not page:
            page_export_structures.append(None)
            continue
        ordered_regions = books.get_ordered_regions(page_id, layout_graph, segments_graph) or []

        segment_export_structures = []
        for region in ordered_regions:
            region_xywh = region['selector']['default']['value'][5:]
            region_bbox = BBOX_RE.match(region_xywh).groupdict()
            region_svg_path_def = SVG_PATH_DEF_RE.findall(region['selector']['item']['value'])[0]

            segment_export_structure = {
                "region_id": region['_id'],
                "region_bbox": region_bbox,
                "region_svg_path_def": region_svg_path_def,
                "segment_url": '{base}/iiif_image/v1/objstore/{page_image_id}/{xywh}/full/0/default.jpg'.format(
                    base=vc.base_url.rstrip('/'), page_image_id=page_image_id, xywh=region_xywh
                )
            }
            anno_ids = region.get('_reached_ids', {}).get('target', None)
            if anno_ids:
                anno_id = anno_ids[0]
                anno = segments_graph[anno_id]

                segment_export_structure.update({
                    "annotation_id": anno_id,
                    "annotation_text": anno['body'][0]['chars'],
                })

            segment_export_structures.append(segment_export_structure)

        del page['_reached_ids']
        page_export_structures.append({
            "page": page,
            "segments": segment_export_structures
        })

    return page_export_structures


def export(vc, book_id, from_page=None, to_page=None):
    """
    It first resolves page_ids from book_id, and pages range.
    if no from, to are given, defaults to all pages.

    Then it splits all those page_ids in batches of some 15,-
    and call above :export_pages: function on each batch, and -
    collects results of each iteration, and reduces to final result.

    To do above batching procedure, we use `batch_it` helper from this package

    finally it returns BIG dict in export structure mentioned in module docstring.
    We can instead save each page in it's own json, or operate on them, to reduce memory usage


    :param vc:
    :param book_id:
    :param from_page:
    :param to_page:
    :return:
    """
    print('1. Resolving page ids')
    pages_range = [from_page, to_page] if (from_page is not None and to_page is not None) else None
    resolved_page_ids = books.resolve_page_ids(vc, book_id, pages_range=pages_range)

    batch_size = 15
    batches_result_reducer = lambda final_result, batch_result: final_result + batch_result  # array concat
    progress_bar = tqdm(
        total=len(resolved_page_ids), desc='2. Exporting pages')
    update_state = lambda state, meta: progress_bar.update(batch_size)

    batched_exporter = batch_it(
        batch_size=batch_size, result_reducer=batches_result_reducer, batch_context=None, update_state=update_state
    )(export_pages)

    pages_export_structures = batched_exporter(resolved_page_ids, vc)
    progress_bar.close()
    return {
        "api_base": vc.base_url,
        "book_id": book_id,
        "pages": pages_export_structures
    }


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--api', help='api base url', dest='api_base', required=True)
    parser.add_argument('-e', '--email', help='user email', dest='email', required=True)
    parser.add_argument('-p', '--pwd', help='password', dest='pwd', required=True)
    parser.add_argument('-b', '--book', help='book_id', dest='book_id', required=True)
    parser.add_argument('-f', '--from', help='from page index', dest='from_page', required=False, type=int)
    parser.add_argument('-t', '--to', help='to page index', dest='to_page', required=False, type=int)
    parser.add_argument('-o', '--output', help='output file', dest='output', required=False)

    args, unknown = parser.parse_known_args()

    vc = VedavaapiSession(args.api_base)
    vc.signin(args.email, args.pwd)

    export_structure = export(vc, args.book_id, from_page=args.from_page, to_page=args.to_page)

    if args.output:
        print('3. Writing to file')
        open(args.output, 'wb').write(
            json.dumps(export_structure, indent=2, ensure_ascii=False, skipkeys=['_reached_ids']).encode('utf-8'))
        print('\nDONE.')
    else:
        print(json.dumps(export_structure, indent=2, ensure_ascii=False, skipkeys=['_reached_ids']))


if __name__ == '__main__':
    sys.exit(main(sys.argv[:]))
